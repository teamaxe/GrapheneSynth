"""
Little script that guesses bonds for a PDB file based on distances, and outputs a new pdb file.
"""
import itertools

from narupa.ase.converter import _bond_threshold
from simtk.openmm.app import PDBFile
import simtk.openmm as mm
import numpy as np

pdb_file_path = "graphene.pdb"
output_file = "graphene_with_bonds.pdb"


def guess_bonds(pdb_file: PDBFile):
    """
    Guess the bonds from the PDB topology
    :param pdb_file: OpenMM PDB file representation
    :return: List of pairs of bond indices
    """
    bonds = []
    print('Generating bonds')
    # there must be a nicer way to do this!
    pair_indices = list(itertools.combinations(range(len(pdb_file.positions)), 2))
    for pair in pair_indices:
        distance = np.linalg.norm(pdb_file.positions[pair[0]] - pdb_file.positions[pair[1]]).value_in_unit(
            mm.unit.angstrom)
        radii = 1.70
        if distance < _bond_threshold([radii, radii]):
            bonds.append([index for index in pair])
    return bonds


def add_bonds(pdb_file: PDBFile):
    """
    Add missing bonds to PDB file, through some guesswork.
    :param pdb_file: OpenMM PDB file representation.
    """
    atoms = [a for a in pdb_file.topology.atoms()]
    for bond in guess_bonds(pdb_file):
        atom1 = atoms[bond[0]]
        atom2 = atoms[bond[1]]
        pdb_file.topology.addBond(atom1, atom2)


pdb_file = PDBFile(pdb_file_path)

# frustratingly, the graphene sheet produced by VMD didn't have bond information, so add them through some distance
# criteria
add_bonds(pdb_file)
with open(output_file, 'w') as file:
    pdb_file.writeFile(pdb_file.topology, pdb_file.positions, file)
