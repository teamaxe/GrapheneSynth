/*
 * Copyright (c) 2020 Tom Mitchell and Alex Jones.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "PluginProcessor.h"
#include "PluginEditor.h"

//==============================================================================
AudioClientAudioProcessorEditor::AudioClientAudioProcessorEditor (AudioClientAudioProcessor& p)
 :  AudioProcessorEditor (&p), 
    processor (p), 
    grapheneComponent (processor.getGrapheneSynthesiser())  

{
    addAndMakeVisible (grapheneComponent);
    setSize (350, 600);
}

AudioClientAudioProcessorEditor::~AudioClientAudioProcessorEditor()
{
}

//==============================================================================
void AudioClientAudioProcessorEditor::paint (Graphics& g)
{

}

void AudioClientAudioProcessorEditor::resized()
{
    grapheneComponent.setBounds (getLocalBounds());
}

