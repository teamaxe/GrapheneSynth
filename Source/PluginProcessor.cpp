/*
 * Copyright (c) 2020 Tom Mitchell and Alex Jones.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "PluginProcessor.h"
#include "PluginEditor.h"

//==============================================================================
AudioClientAudioProcessor::AudioClientAudioProcessor()
#ifndef JucePlugin_PreferredChannelConfigurations
     :  AudioProcessor (BusesProperties()
                     #if ! JucePlugin_IsMidiEffect
                      #if ! JucePlugin_IsSynth
                       .withInput  ("Input",  AudioChannelSet::stereo(), true)
                      #endif
                       .withOutput ("Output", AudioChannelSet::stereo(), true)
                     #endif
                       ), 
        PEwindow (50), 
        KEwindow (50)
    
#endif
{
    std::vector<NormalisableRange<float>>& ranges = grapheneSynthesiser.getSimulationParameterRanges();

    addParameter(temperature = new AudioParameterFloat("temp", "Temp", ranges[temperatureID], 1.0f));
    addParameter(friction = new AudioParameterFloat("friction", "Friction", ranges[frictionID], 0.1f));
    addParameter(timestep = new AudioParameterFloat("timestep", "Timestep", ranges[timestepID], 0.01f));

    grapheneSynthesiser.getGrapheneSimulation().setKineticEnergyCB([&](float value)
        {
            const auto windowVal = KEwindow.getNormalisedValue(value);
            const auto clamp = std::clamp(windowVal, 0.f, 1.f);

            kineticEnergy = std::powf(clamp, 3);

            simulationValues.set(energyKinetic, kineticEnergy.load());
        });
    addListener(&grapheneSynthesiser);
}

AudioClientAudioProcessor::~AudioClientAudioProcessor()
{
    
}
//==============================================================================
GrapheneSynthesiser& AudioClientAudioProcessor::getGrapheneSynthesiser()
{
    return grapheneSynthesiser;
}

//==============================================================================
const String AudioClientAudioProcessor::getName() const
{
    return JucePlugin_Name;
}

bool AudioClientAudioProcessor::acceptsMidi() const
{
   #if JucePlugin_WantsMidiInput
    return true;
   #else
    return false;
   #endif
}

bool AudioClientAudioProcessor::producesMidi() const
{
   #if JucePlugin_ProducesMidiOutput
    return true;
   #else
    return false;
   #endif
}

bool AudioClientAudioProcessor::isMidiEffect() const
{
   #if JucePlugin_IsMidiEffect
    return true;
   #else
    return false;
   #endif
}

double AudioClientAudioProcessor::getTailLengthSeconds() const
{
    return 0.0;
}

int AudioClientAudioProcessor::getNumPrograms()
{
    return 1;   // NB: some hosts don't cope very well if you tell them there are 0 programs,
                // so this should be at least 1, even if you're not really implementing programs.
}

int AudioClientAudioProcessor::getCurrentProgram()
{
    return 0;
}

void AudioClientAudioProcessor::setCurrentProgram (int index)
{
}

const String AudioClientAudioProcessor::getProgramName (int index)
{
    return {};
}

void AudioClientAudioProcessor::changeProgramName (int index, const String& newName)
{
}

//==============================================================================
void AudioClientAudioProcessor::prepareToPlay (double sampleRate, int samplesPerBlock)
{
    grapheneSynthesiser.prepareToPlay(sampleRate, samplesPerBlock);
}

void AudioClientAudioProcessor::releaseResources()
{
}

#ifndef JucePlugin_PreferredChannelConfigurations
bool AudioClientAudioProcessor::isBusesLayoutSupported (const BusesLayout& layouts) const
{
  #if JucePlugin_IsMidiEffect
    ignoreUnused (layouts);
    return true;
  #else
    // This is the place where you check if the layout is supported.
    // In this template code we only support mono or stereo.
    if (layouts.getMainOutputChannelSet() != AudioChannelSet::mono()
     && layouts.getMainOutputChannelSet() != AudioChannelSet::stereo())
        return false;

    // This checks if the input layout matches the output layout
   #if ! JucePlugin_IsSynth
    if (layouts.getMainOutputChannelSet() != layouts.getMainInputChannelSet())
        return false;
   #endif

    return true;
  #endif
}
#endif

void AudioClientAudioProcessor::processBlock (AudioBuffer<float>& buffer, MidiBuffer& midiMessages)
{
    ScopedNoDenormals noDenormals;
    
    processMappings(midiMessages);

	grapheneSynthesiser.processBlock (buffer, midiMessages);
}
;
//==============================================================================
bool AudioClientAudioProcessor::hasEditor() const
{
    return true; // (change this to false if you choose to not supply an editor)
}

AudioProcessorEditor* AudioClientAudioProcessor::createEditor()
{
    return new AudioClientAudioProcessorEditor (*this);
}

void AudioClientAudioProcessor::processMappings(MidiBuffer& midiMessages)
{
    MidiBuffer::Iterator iterator(midiMessages);
    MidiMessage currentMessage;
    int sampleNumber;

    // check midi to simulation mappings
    //iterate over the incoming midi messages
    while (iterator.getNextEvent(currentMessage, sampleNumber))
    {
        //create a description of the midi message
        MidiData midiData = getMessageData(currentMessage);

        // if a mapping exists for this midi message
        if (grapheneSynthesiser.getMappings().getMidiToSimulationMappings().count(midiData))
        {
            // prepare a message to send to the server based on the mapping details
            String id = grapheneSynthesiser.getMappings().getMidiToSimulationMappings().find(midiData)->second.simulationID;
            auto value = getMessageValue(currentMessage);
            // send a message to the server
        }
    }

    //check simulation to midi mappings
    //iterate over the simulation parameter mappings
    std::map<String, Mapping> m(grapheneSynthesiser.getMappings().getSimulationtoMidiMappings());

    for (std::map<String, Mapping>::iterator j(m.begin()); j != m.end(); j++)
    {
        //if there is a mapping for this simulation parameter (no need for an if statement as all the elements in this map are mappings) 
        String id = j->first;
        // get the current vaule of the parameter in question
        auto simVal = simulationValues[id];

        //create a midi message based on the settings of that particular mapping and the current simulation value
        auto range = j->second.range;
        auto center = j->second.centre;

        auto value = (simVal * range) + center;

        std::clamp<float>(value, 0, 1);

        auto midiValue = value * 127.0f;
        auto channel = j->second.midiData.getChannel();
        auto number = j->second.midiData.getNumber();

        // add the message to the midi buffer
        auto message = MidiMessage::controllerEvent(channel, number, value);
        addMessageToBuffer(message, midiMessages, getSampleRate());
    }
}

void AudioClientAudioProcessor::addMessageToBuffer(const MidiMessage& message, MidiBuffer& buffer, double sampleRate)
{
    auto timestamp = message.getTimeStamp();
    auto sampleNumber = (int)(timestamp * sampleRate);
    buffer.addEvent(message, sampleNumber);
}

MidiData AudioClientAudioProcessor::getMessageData(MidiMessage m)
{
    int channel = m.getChannel();
    int number = 0;
    int type = 0;

    if (m.isController())
    {
        type = Mappings::midiType::cc;
        number = m.getControllerNumber();
    }
    else if (m.isNoteOnOrOff())
    {
        type = Mappings::midiType::note;
        number = m.getNoteNumber();
    }

    MidiData result(number, type, channel);
    return result;
}

float AudioClientAudioProcessor::getMessageValue(MidiMessage m)
{
    if (m.isNoteOnOrOff())
    {
        return m.getNoteNumber();
    }
    else if (m.isController())
    {
        return m.getControllerValue();
    }
}
//==============================================================================
void AudioClientAudioProcessor::getStateInformation (MemoryBlock& destData)
{
    copyXmlToBinary(grapheneSynthesiser.getMappings().getXMLDescription(), destData);
}

void AudioClientAudioProcessor::setStateInformation (const void* data, int sizeInBytes)
{
    std::unique_ptr<XmlElement> xmlState(getXmlFromBinary(data, sizeInBytes));
    if (xmlState.get() != nullptr)
    {
        if (xmlState->hasTagName(mainDocTag))
        {
            grapheneSynthesiser.getMappings().setFromXML(*xmlState);
        }
    }
}

//==============================================================================
// This creates new instances of the plugin..
AudioProcessor* JUCE_CALLTYPE createPluginFilter()
{
    return new AudioClientAudioProcessor();
}
