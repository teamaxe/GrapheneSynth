/*
 * Copyright (c) 2020 Tom Mitchell and Alex Jones.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include "../JuceLibraryCode/JuceHeader.h"
#include "Graphene/GrapheneSynthesiser.h"
#include "utility/Mappings.h"
#include "utility/Window.h"

//==============================================================================
/**
*/
class AudioClientAudioProcessor  : public AudioProcessor
{
public:
    //==============================================================================
    AudioClientAudioProcessor();
    ~AudioClientAudioProcessor();

    //==============================================================================
    GrapheneSynthesiser& getGrapheneSynthesiser();

    //==============================================================================
    void prepareToPlay (double sampleRate, int samplesPerBlock) override;
    void releaseResources() override;

   #ifndef JucePlugin_PreferredChannelConfigurations
    bool isBusesLayoutSupported (const BusesLayout& layouts) const override;
   #endif

    void processBlock (AudioBuffer<float>&, MidiBuffer&) override;

    //==============================================================================
    AudioProcessorEditor* createEditor() override;
    bool hasEditor() const override;

    //==============================================================================
    const String getName() const override;

    bool acceptsMidi() const override;
    bool producesMidi() const override;
    bool isMidiEffect() const override;
    double getTailLengthSeconds() const override;

    //==============================================================================
    int getNumPrograms() override;
    int getCurrentProgram() override;
    void setCurrentProgram (int index) override;
    const String getProgramName (int index) override;
    void changeProgramName (int index, const String& newName) override;

    //==============================================================================

    void processMappings(MidiBuffer& midiMessages);

    void addMessageToBuffer(const MidiMessage& message, MidiBuffer& buffer, double sampleRate);

    MidiData getMessageData(MidiMessage);
    float getMessageValue(MidiMessage);

    Mappings& getMappings() { return grapheneSynthesiser.getMappings(); }

    //==============================================================================
    void getStateInformation (MemoryBlock& destData) override;
    void setStateInformation (const void* data, int sizeInBytes) override;

private:
    GrapheneSynthesiser grapheneSynthesiser;

    AudioParameterFloat* temperature;
    AudioParameterFloat* friction;
    AudioParameterFloat* timestep;

    Window KEwindow;
    Window PEwindow;

    std::atomic<float> kineticEnergy{ 0.f };
    std::atomic<float> potentialEnergy{ 0.f };

    //Mappings mappings;
    HashMap<String, double> simulationValues;
    //==============================================================================
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (AudioClientAudioProcessor)
};
