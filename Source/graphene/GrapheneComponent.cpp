/*
 * Copyright (c) 2020 Tom Mitchell and Alex Jones.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "GrapheneComponent.h"

GrapheneVisualisation::GrapheneVisualisation(GrapheneSynthesiser& gs) : grapheneSynthesiser(gs)
{
    startTimerHz(20);
}

GrapheneVisualisation::~GrapheneVisualisation()
{

}

void GrapheneVisualisation::paint (Graphics& g)
{
    // (Our component is opaque, so we must completely fill the background with a solid colour)
    g.fillAll(getLookAndFeel().findColour(ResizableWindow::backgroundColourId));
    g.setColour(Colours::white);

    if (!ranges[0].isEmpty())
    {
        const auto diameter = getWidth() / 42;
        juce::Rectangle<int> atomArea({ 0, 0, diameter, diameter });
        auto coordinateCounter = 0;
        while (coordinateCounter < positions.size())
        {
            auto x = positions[coordinateCounter++];
            auto y = positions[coordinateCounter++];
            auto z = positions[coordinateCounter++];

            auto xcentre = coordinateToPixel (Axis::X, x);
            auto ycentre = coordinateToPixel (Axis::Y, y);

            auto rect = atomArea.withCentre ({ coordinateToPixel (Axis::X, x), coordinateToPixel (Axis::Y,y) }).toFloat();

            g.fillEllipse(rect);
        }

        g.setColour(Colours::hotpink);
        for (auto index : GraphenePath::getPath (GraphenePath::PathType::Hex1))
        {
            auto offset = index * 3;
            auto x = positions[offset++];
            auto y = positions[offset++];
            auto z = positions[offset];

            auto xcentre = coordinateToPixel (Axis::X, x);
            auto ycentre = coordinateToPixel (Axis::Y, y);

            auto rect = atomArea.withCentre ({ coordinateToPixel (Axis::X, x), coordinateToPixel (Axis::Y,y) }).toFloat();

            g.fillEllipse(rect);
        }

        g.setColour(Colours::lightgreen);
        for (auto index : GraphenePath::getPath (GraphenePath::PathType::Hex5))
        {
            auto offset = index * 3;
            auto x = positions[offset++];
            auto y = positions[offset++];
            auto z = positions[offset];

            auto xcentre = coordinateToPixel (Axis::X, x);
            auto ycentre = coordinateToPixel (Axis::Y, y);

            auto rect = atomArea.withCentre ({ coordinateToPixel(Axis::X, x), coordinateToPixel(Axis::Y,y) }).toFloat();

            g.fillEllipse(rect);
        }
        g.setColour(Colours::red);
        {
        	const auto centroid = grapheneSynthesiser.getCentroid();
        	auto x = centroid[0];
        	auto y = centroid[1];
        	auto z = centroid[2];

        	auto xcentre = coordinateToPixel (Axis::X, x);
        	auto ycentre = coordinateToPixel (Axis::Y, y);

        	auto rect = atomArea.withCentre({ coordinateToPixel (Axis::X, x), coordinateToPixel (Axis::Y,y) }).toFloat();

        	g.fillEllipse(rect);
        }
    }
    else if (positions.size() > 0)
    {
        auto axisCounter = 0;
        ranges[(int)Axis::X] = Range<float>(positions[(int)Axis::X], positions[(int)Axis::X]);
        ranges[(int)Axis::Y] = Range<float>(positions[(int)Axis::Y], positions[(int)Axis::Y]);
        ranges[(int)Axis::Z] = Range<float>(positions[(int)Axis::Z], positions[(int)Axis::Z]);

        for (auto coordinate : positions)
        {
            auto& range = ranges[axisCounter];
            if (coordinate > range.getEnd())
                range.setEnd(coordinate);
            else if (coordinate < range.getStart())
                range.setStart(coordinate);

            if (++axisCounter > 2)
                axisCounter = 0;
        }
    }
}

int GrapheneVisualisation::coordinateToPixel (Axis axis, float value) const
{
    auto& range = axis == Axis::X ? ranges[(int)Axis::X] : axis == Axis::Y ? ranges[(int)Axis::Y] : ranges[(int)Axis::Z];
    auto pixelLength = axis == Axis::X ? getWidth() - 50 : getHeight() - 50;
    float scaleFactor = pixelLength / range.getLength();
    auto position = (value - range.getStart()) * scaleFactor + 25;

    if (axis == Axis::Y)
        return getHeight() - position;

    return position;

}

void GrapheneVisualisation::timerCallback()
{
    grapheneSynthesiser.getPositionsCopy (positions);
    repaint();
}

GrapheneControls::GrapheneControls (GrapheneSimulation& gs) : grapheneSimulation (gs)
{
    temperatureSlider.setRange ({ 0.0, 10000.0 }, 1);
    frictionSlider.setRange    ({ 0.01, 100.0 }, 0.1);
    timestepSlider.setRange    ({ 0.01, 1.5 }, 0.01);
    //set to defaults
    temperatureSlider.setValue (300.0, NotificationType::dontSendNotification);
    frictionSlider.setValue    (1.0, NotificationType::dontSendNotification);
    timestepSlider.setValue    (0.5, NotificationType::dontSendNotification);

    temperatureSlider.setSkewFactor (0.7);
    frictionSlider.setSkewFactor    (0.7);
    timestepSlider.setSkewFactor    (0.7);

    temperatureSlider.setSliderStyle (Slider::LinearHorizontal);
    frictionSlider.setSliderStyle    (Slider::LinearHorizontal);
    timestepSlider.setSliderStyle    (Slider::LinearHorizontal);

    const int sliderTextBoxWidth  = 40;
    const int sliderTextBoxHeight = 30;

    temperatureSlider.setTextBoxStyle (Slider::TextBoxLeft, false, sliderTextBoxWidth, sliderTextBoxHeight);
    frictionSlider.setTextBoxStyle    (Slider::TextBoxLeft, false, sliderTextBoxWidth, sliderTextBoxHeight);
    timestepSlider.setTextBoxStyle    (Slider::TextBoxLeft, false, sliderTextBoxWidth, sliderTextBoxHeight);

    Colour theme = Colours::aquamarine.interpolatedWith (Colours::lightgrey, 0.2);

    temperatureSlider.setColour (Slider::thumbColourId, theme);
    frictionSlider.setColour    (Slider::thumbColourId, theme);
    timestepSlider.setColour    (Slider::thumbColourId, theme);

    temperatureLabel.setColour (Label::textColourId, theme);
    frictionLabel.setColour    (Label::textColourId, theme);
    timestepLabel.setColour    (Label::textColourId, theme);

    addAndMakeVisible (temperatureSlider);
    addAndMakeVisible (frictionSlider);
    addAndMakeVisible (timestepSlider);

    addAndMakeVisible (temperatureLabel);
    addAndMakeVisible (frictionLabel);
    addAndMakeVisible (timestepLabel);

    temperatureSlider.onValueChange = [this]()
    {
        grapheneSimulation.setTemperature (temperatureSlider.getValue());
    };

    frictionSlider.onValueChange = [this]()
    {
        grapheneSimulation.setFriction (frictionSlider.getValue());
    };

    timestepSlider.onValueChange = [this]()
    {
        grapheneSimulation.setTimestep (timestepSlider.getValue());
    };

    addAndMakeVisible (pluckButton);
    pluckButton.onClick = [this]()
    {
        grapheneSimulation.pluck();
    };
}

GrapheneControls::~GrapheneControls()
{

}

void GrapheneControls::resized()
{
    auto r = getLocalBounds().reduced(3);
    
    auto buttonArea = r.removeFromBottom(r.getHeight() * 0.2);
    float w = buttonArea.getWidth() / 3;
    pluckButton.setBounds(buttonArea.removeFromLeft(w).removeFromRight(w));

    auto labelArea = r.removeFromLeft(r.getWidth() * 0.2);
    auto sliderArea = r;

    const auto unitHeight = sliderArea.getHeight() / 3.0;

    temperatureSlider.setBounds (sliderArea.removeFromTop (unitHeight));
    frictionSlider.setBounds (sliderArea.removeFromTop (unitHeight));
    timestepSlider.setBounds (sliderArea);

    temperatureLabel.setBounds(labelArea.removeFromTop(unitHeight));
    frictionLabel.setBounds(labelArea.removeFromTop(unitHeight));
    timestepLabel.setBounds(labelArea);
}

void GrapheneControls::paint(Graphics& g)
{
    g.setColour(Colours::aquamarine.interpolatedWith(Colours::lightgrey, 0.3));
    g.drawRect(getLocalBounds(), 2.0);
}

AdsrControls::AdsrControls(GrapheneSynthesiser& gs) : grapheneSynth(gs)
{
    
    for (auto i = 0; i < NumParameters; i++)
    {
        paramSliders.push_back (std::move (std::make_unique<Slider> (ParameterNames[i])));
        paramLabels.push_back (std::move (std::make_unique<Label> (ParameterNames[i], ShortNames[i])));
        paramLabels[i]->setColour (Label::textColourId, Colours::red.interpolatedWith (Colours::darkgrey, 0.5));
        paramLabels[i]->getFont().setBold(true);
        paramLabels[i]->setJustificationType (Justification::centred);
        paramSliders[i]->setSliderStyle (Slider::RotaryVerticalDrag);
        paramSliders[i]->setTextBoxStyle (Slider::TextBoxBelow, false, 45, 13);
        paramSliders[i]->setColour (Slider::thumbColourId, Colours::red.interpolatedWith(Colours::darkgrey, 0.5));

        addAndMakeVisible (paramSliders[i].get());
        addAndMakeVisible (paramLabels[i].get());
    }
    paramSliders[Attack] ->setRange ({ 0.0, 2.0 }, 0.0001);
    paramSliders[Decay]  ->setRange ({ 0.0, 2.0 }, 0.0001);
    paramSliders[Sustain]->setRange ({ 0.0, 1.0 }   , 0.0001);
    paramSliders[Release]->setRange ({ 0.0, 10.0 }, 0.0001);

    paramSliders[Attack]->setValue (0.1, dontSendNotification);
    paramSliders[Decay]->setValue (0.2, dontSendNotification);
    paramSliders[Sustain]->setValue (0.9, dontSendNotification);
    paramSliders[Release]->setValue (0.5, dontSendNotification);

    paramSliders[Attack]->onValueChange = [this]()
    {
        grapheneSynth.setAttack(paramSliders[Attack]->getValue());
    };
    paramSliders[Decay]->onValueChange = [this]()
    {
        grapheneSynth.setDecay (paramSliders[Decay]->getValue());
    };
    paramSliders[Sustain]->onValueChange = [this]()
    {
        grapheneSynth.setSustain(paramSliders[Sustain]->getValue());
    };
    paramSliders[Release]->onValueChange = [this]()
    {
        grapheneSynth.setRelease(paramSliders[Release]->getValue());
    };
}

AdsrControls::~AdsrControls()
{

}

void AdsrControls::resized()
{
    Rectangle<int> r = getLocalBounds().reduced (3);
    Rectangle<int> labelArea = r.removeFromTop (r.getHeight() * 0.15);
    Rectangle<int> sliderArea = r;

    auto unitWidth = labelArea.getWidth() / paramLabels.size();

    for (auto& s : paramSliders)
        s->setBounds (sliderArea.removeFromLeft (unitWidth));

    for (auto& l : paramLabels)
        l->setBounds (labelArea.removeFromLeft (unitWidth));
}

void AdsrControls::paint (Graphics& g)
{
    g.setColour (Colours::red.interpolatedWith (Colours::darkgrey, 0.75));
    g.drawRect (getLocalBounds(), 2.0);
}

WavetableComponent::WavetableComponent (GrapheneSynthesiser& synthesiser) : grapheneSynthesiser (synthesiser)
{
    startTimerHz (20);
}

WavetableComponent::~WavetableComponent()
{

}

void WavetableComponent::paint (Graphics& g)
{
    if (wavetable.size() == 0)
        return;

    g.setColour (Colours::white);

    const auto width = static_cast<float> (getWidth());

    const auto minValue = -1.f; //plot min and max values
    const auto maxValue = 1.f;
    const auto yValueScaleFactor = static_cast<float>(getHeight()) / (maxValue - minValue);

    const auto pixelsPerElement = width / static_cast<float>(wavetable.size() - 1);

    for (int i = 0; i < wavetable.size() - 1; i++)
    {
        const auto startValue = wavetable[i];
        const auto endValue = wavetable[i + 1];

        const auto ystart = (startValue + minValue) * -yValueScaleFactor;
        const auto yend = (endValue + minValue) * -yValueScaleFactor;

        const auto xstart = i * pixelsPerElement;
        const auto xend = (i + 1) * pixelsPerElement;

        g.drawLine ({xstart,ystart,xend,yend});  
    }
}

void WavetableComponent::timerCallback()
{
    grapheneSynthesiser.getWavetableCopy (wavetable);
    repaint();
}

GrapheneComponent::GrapheneComponent (GrapheneSynthesiser& gs) :    
                                                                    grapheneVisualisation (gs),
                                                                    grapheneControls (gs.getGrapheneSimulation()),
                                                                    wavetableComponent (gs),
                                                                    adsrControls(gs),
                                                                    mappingsTable(gs.getMappings())
{
    addAndMakeVisible (grapheneVisualisation);
    addAndMakeVisible (grapheneControls);
    addAndMakeVisible (wavetableComponent);
    addAndMakeVisible (adsrControls);
    addAndMakeVisible (mappingTableStateButton);
    addAndMakeVisible (mappingTableLabel);

    mappingTableLabel.setJustificationType (Justification::centred);
    mappingTableLabel.setText ("Toggle mappings menu", dontSendNotification);
    mappingTableStateButton.addListener (this);
}

GrapheneComponent::~GrapheneComponent()
{

}

//Component
void GrapheneComponent::resized()
{
    Rectangle<int> r (getLocalBounds());

    Rectangle<int> mappingSelectArea = r.removeFromTop (r.getHeight() * 0.03);
    mappingTableStateButton.setBounds (mappingSelectArea.removeFromLeft (25));
    mappingTableLabel.setBounds (mappingSelectArea.removeFromLeft(mappingSelectArea.getWidth() * 0.5));

    mappingsTable.setBounds (r);
    grapheneVisualisation.setBounds (r.removeFromTop (getWidth()));
    grapheneControls.setBounds (r.removeFromTop (r.getHeight() * 0.35));
    adsrControls.setBounds (r.removeFromTop (r.getHeight() * 0.5));
    wavetableComponent.setBounds (r);  
}

void GrapheneComponent::buttonClicked (Button* b)
{
    if (b->getToggleState())
    {
        getParentComponent()->setSize (730, 600);
        grapheneVisualisation.setVisible (false);
        addAndMakeVisible (mappingsTable);
    }
    else if (!b->getToggleState())
    {
        getParentComponent()->setSize (350, 600);
        grapheneVisualisation.setVisible (true);
        removeChildComponent (&mappingsTable);
    }
}

