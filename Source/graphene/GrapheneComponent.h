/*
 * Copyright (c) 2020 Tom Mitchell and Alex Jones.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include "../JuceLibraryCode/JuceHeader.h"
#include "GrapheneSynthesiser.h"
#include <array>
#include "../utility/MappingsTable.h"

class GrapheneVisualisation :   public	Component,
                                private Timer
{
public:
    GrapheneVisualisation (GrapheneSynthesiser& gs);
    ~GrapheneVisualisation();

    //Component
    void paint(Graphics&) override;

private:
    GrapheneVisualisation() = default;
    enum class Axis
    {
    	X,
    	Y,
    	Z
    };

    int coordinateToPixel (Axis axis, float value) const;
    void timerCallback() override;

    GrapheneSynthesiser& grapheneSynthesiser;

    std::array<Range<float>, 3> ranges;
    std::vector<float> positions;
};

class GrapheneControls : public	Component
{
public:
    GrapheneControls (GrapheneSimulation& gs);
    ~GrapheneControls();

    //Component
    void resized() override;
    void paint(Graphics& g) override;
private:
    GrapheneControls() = default;
    GrapheneSimulation& grapheneSimulation;
    Slider temperatureSlider {"Temperature "};
    Slider frictionSlider { "Friction " };
    Slider timestepSlider { "Timestep " };
    Label temperatureLabel{"Temperature", "Temperature" };
    Label frictionLabel{ "Friction", "Friction" };
    Label timestepLabel{ "TimeStep", "TimeStep" };
      
    TextButton pluckButton {"Pluck"};
};

class AdsrControls : public	Component
{
public:
    AdsrControls(GrapheneSynthesiser& gs);
    ~AdsrControls();
    void resized() override;
    void paint(Graphics& g) override;

private:
    AdsrControls() = default;
    GrapheneSynthesiser& grapheneSynth;
    enum ParamName                      { Attack ,  Decay ,  Sustain ,  Release, NumParameters };
    const StringArray ParameterNames    {"Attack", "Decay", "Sustain", "Release"};
    const StringArray ShortNames        {  "A"   ,   "D"  ,    "S"   ,    "R"   };
    std::vector<std::unique_ptr<Slider>> paramSliders;
    std::vector <std::unique_ptr<Label>> paramLabels;
};

class WavetableComponent :	public Component,
							private Timer
{
public:
    WavetableComponent (GrapheneSynthesiser& gs);
    ~WavetableComponent();

    //Component
    void paint(Graphics&) override;
private:
    WavetableComponent() = default;
    void timerCallback() override;

    GrapheneSynthesiser& grapheneSynthesiser;
    std::vector<float> wavetable;
};


class GrapheneComponent : public Component, public Button::Listener
{
public:
    GrapheneComponent (GrapheneSynthesiser& gs);
    ~GrapheneComponent();

    //Component
    void resized() override;

    void buttonClicked(Button* b) override;

private:
    GrapheneComponent() = default;
    GrapheneVisualisation grapheneVisualisation;
    GrapheneControls grapheneControls;
    WavetableComponent wavetableComponent;
    AdsrControls adsrControls;
    MappingsTable mappingsTable;

    ToggleButton mappingTableStateButton;
    Label mappingTableLabel;
};