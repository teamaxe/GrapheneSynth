/*
 * Copyright (c) 2020 Tom Mitchell and Alex Jones.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "GrapheneSimulation.h"

const std::vector<int> GraphenePath::getPath (PathType pathType)
{
    //must be in this range
    switch (pathType)
    {
        case PathType::Hex1:        return { 339, 341, 378, 379, 380, 381 };
        case PathType::Hex2:        return { 299, 301, 336, 337, 338, 340, 342, 343, 376, 377, 382, 383, 416, 418, 419, 420, 421, 422 };
        case PathType::Hex3:        return { 259, 261, 295, 296, 297, 298, 300, 302, 303, 305, 334, 335, 344, 345, 374, 375, 384, 385, 414, 415, 417, 423, 424, 425, 456, 458, 459, 460, 461, 462 };
        case PathType::Hex4:        return { 219, 221, 255, 256, 258, 260, 262, 263, 265, 292, 293, 294, 304, 306, 307, 332, 333, 346, 347, 372, 373, 386, 387, 412, 413, 426, 427, 452, 454, 455, 457, 463, 464, 465, 466, 496, 498, 499, 500, 501, 502 };
        case PathType::Hex5:        return { 541, 540, 542, 503, 505, 504, 506, 467, 469, 468, 429, 428, 389, 388, 349, 348, 309, 308, 269, 267, 266, 264, 225, 223, 222, 220, 181, 179, 218, 216, 217, 215, 254, 252, 253, 251, 290, 291, 330, 331, 370, 371, 410, 411, 450, 451, 453, 492, 494, 495, 497, 536, 538, 539 };
        case PathType::LineDiag1:   return { 521, 522, 523, 268, 269, 270, 271, 412, 413, 414, 415, 304, 305, 306, 307, 448, 449, 450, 451, 196, 197, 198, 199, 340, 341, 342, 343, 484, 485, 486, 487, 232, 233, 234, 235, 376, 377, 378, 379 };
    }
}

GrapheneSimulation::GrapheneSimulation() : Thread ("InteractionEventThread")
{
    trajectoryService.setPositionCallback ([this](std::vector<float> newPositions)
    {
        {
            std::lock_guard<std::mutex> lg (positionsLock);
            positions = newPositions;
        }
        {
            std::lock_guard<std::mutex> lg(listenerLock);
            listeners.call(&Listener::simulationUpdate, newPositions);
        }
    });
    trajectoryService.connect ("localhost");
    commandService.connect ("localhost");
    imdService.connect ("localhost");
    startThread();
}

GrapheneSimulation::~GrapheneSimulation()
{
    stopThread (1000);
}

int GrapheneSimulation::getParticleCount()
{
    std::lock_guard<std::mutex> lg (positionsLock);
    return positions.size() / 3;
}

void GrapheneSimulation::pluck (float scale, int durationMs)
{ 
    int64 currentTime = Time::currentTimeMillis();
    const std::string id = std::to_string (idGenerator.get());
    ImdService::Interaction interaction (id, { 0.f, 0.f, 0.f }, 468, scale, ImdService::Interaction::ForceType::Spring);
    addEvent (currentTime, interaction);
    addEvent (currentTime + durationMs, interaction, true);
}

void GrapheneSimulation::getPositionsCopy (std::vector<float>& containerForPositionsCopy)
{
    std::lock_guard<std::mutex> lg (positionsLock);
    containerForPositionsCopy = positions;
}

void GrapheneSimulation::addListener (GrapheneSimulation::Listener* listenerToAdd)
{
    std::lock_guard<std::mutex> lg (listenerLock);
    listeners.add (listenerToAdd);
}

void GrapheneSimulation::removeListener (GrapheneSimulation::Listener* listenerToRemove)
{
    std::lock_guard<std::mutex> lg (listenerLock);
    listeners.remove (listenerToRemove);
}

void GrapheneSimulation::run()
{
    while (! threadShouldExit())
    {
        int waitTime = 100;
        {
            std::lock_guard<std::mutex> lg (eventListLock);

            for (auto iter = eventList.begin(); iter != eventList.end(); )
            {
                auto eventTime = iter->time;
                auto currentTime = Time::currentTimeMillis();

                if (currentTime >= eventTime)
                {
                    if (iter->endStream)
                        imdService.endInteraction (iter->interaction.id);
                    else
                        imdService.beginOrUpdateInteration (iter->interaction);

                    iter = eventList.erase (iter);
                }
                else
                {
                    waitTime = eventTime - currentTime;
                    ++iter;
                    break; //leave subsequent events
                }
            }
        }
        wait (waitTime);
    }
}
