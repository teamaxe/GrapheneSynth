/*
 * Copyright (c) 2020 Tom Mitchell and Alex Jones.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include "../JuceLibraryCode/JuceHeader.h"
#include "../narupacppclient/NarupaClient.h"

#include <mutex>
#include <set>

class GraphenePath
{
public:
    GraphenePath() = default;
    /** returns a list of atom indices that reference a ring of atoms path 1 is the innermost hexagon */
    enum class PathType
    {
        Hex1,
        Hex2,
        Hex3,
        Hex4,
        Hex5,
        LineDiag1
    };
    static const std::vector<int> getPath (PathType pathType); 
private:
};

class GrapheneSimulation : private Thread
{
public:
    GrapheneSimulation();
    ~GrapheneSimulation();

    int getParticleCount();
    void getPositionsCopy (std::vector<float>& containerForPositionsCopy);
    void setTemperature (double newTemperature) { commandService.setTemperature (newTemperature); }
    void setFriction (double newFriction) { commandService.setFriction (newFriction); }
    void setTimestep (double newTimestep) { commandService.setTimestep (newTimestep); }

    void setKineticEnergyCB(std::function<void(float kinEnergy)> callback)
    {
        trajectoryService.setKinEnergyCallback(callback);
    }

    /** scale = 100 is stable */
    void pluck (float scale, int durationMs);
    void pluck() { pluck (100, 500); };

    class Listener
    {
    public:
        Listener() = default;
        virtual ~Listener() = default;
        virtual void simulationUpdate (const std::vector<float> newPositions) = 0;
    };
    void addListener (GrapheneSimulation::Listener* listenerToAdd);
    void removeListener (GrapheneSimulation::Listener* listenerToRemove);

private:
    //thread for event dispatching
    void run() override;

    struct UniqueIdGenerator
    {
        int64 get()
        {
            static int64 uniqueId = 0;
            return uniqueId++;
        }
    };

    UniqueIdGenerator idGenerator;

    struct InteractionEvent
    {
        InteractionEvent (int64 t, const ImdService::Interaction& i, bool es)
            : time(t), interaction(i), endStream(es) {}
        int64 time{ 0 };
        ImdService::Interaction interaction;
        bool endStream{ false };
    };

    std::mutex eventListLock;
    std::vector<InteractionEvent> eventList;

    void addEvent (int64 timeForEvent, const ImdService::Interaction& eventToAdd)
    {
        addEvent (timeForEvent, eventToAdd, false);
    }

    void addEvent (int64 timeForEvent, const ImdService::Interaction& eventToAdd, bool isLastStreamEvent)
    {
        std::lock_guard<std::mutex> lg (eventListLock);
        //insert in time order
        auto it = std::lower_bound(eventList.begin(), eventList.end(), timeForEvent, [](const InteractionEvent& lhs, int64 rhs) -> bool
        { return lhs.time < rhs; });
        eventList.emplace (it, timeForEvent, eventToAdd, isLastStreamEvent); // insert before iterator it
        notify(); //wake up the event dispatcher
    }

    TrajectoryService trajectoryService;
    CommandService commandService;
    ImdService imdService;
    std::mutex positionsLock;
    std::vector<float> positions;

    std::mutex listenerLock;
    ListenerList<GrapheneSimulation::Listener> listeners;

    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (GrapheneSimulation)
};
