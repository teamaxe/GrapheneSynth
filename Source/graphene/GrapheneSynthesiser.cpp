/*
 * Copyright (c) 2020 Tom Mitchell and Alex Jones.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "GrapheneSynthesiser.h"
#define _USE_MATH_DEFINES
#include <math.h>

//==============================================================================
/** Our demo synth sound is just a basic sine wave.. */
struct WavetableSound : public SynthesiserSound
{
    WavetableSound() {}

    bool appliesToNote (int /*midiNoteNumber*/) override { return true; }
    bool appliesToChannel (int /*midiChannel*/) override { return true; }
};

WavetableVoice::WavetableVoice (std::array<std::vector<float>, NumWavetables>& w, std::atomic<int>& wi, int sps)
 :  wavetables (w), 
    wavetableIndex (wi), 
    scanPathSize (sps)
{
    setFrequency (220.0f);
   
    envelope.setParameters (envelopeParams);
}

void WavetableVoice::setFrequency (float newFrequency)
{
    elementsPerSample = (newFrequency * (float)scanPathSize) / ((float)getSampleRate() * (float)OversampleAmount);
}

float WavetableVoice::getNextSample()
{
    std::array<float, OversampleAmount> oversampleBuffer;

    for (auto& sample : oversampleBuffer)
    {
        auto firstElement = static_cast<int> (elementPosition);
        auto secondElement = firstElement + 1;
        if (secondElement >= scanPathSize)
            secondElement -= scanPathSize;

        auto firstCoeficient = 1.f - (elementPosition - static_cast<float>(firstElement));
        auto secondCoificient = 1.f - firstCoeficient;

        sample = firstCoeficient * wavetables[currentWavetable][firstElement] + secondCoificient * wavetables[currentWavetable][secondElement];

        if (wavefade < 1.f)
        {
            auto sampleNext = firstCoeficient * wavetables[nextWavetable][firstElement] + secondCoificient * wavetables[nextWavetable][secondElement];

            sample = wavefade * sampleNext + (1.f - wavefade) * sample;
            wavefade += wavefadeRate;
            if (wavefade >= 1.f)
                currentWavetable = nextWavetable;
        }

        sample = dcBlocker.processSample (sample) * level;

        elementPosition += elementsPerSample;
        if (elementPosition >= static_cast<float>(scanPathSize))
            elementPosition -= static_cast<float>(scanPathSize);
    }
    float output;
    lagrangeInterpolator.process ((double)OversampleAmount, oversampleBuffer.data(), &output, 1);
    return output;
}

void WavetableVoice::getNextFrame (float* frame, int frameSize)
{
    for (auto i = 0; i < frameSize; i++)
    {
        auto firstElement = static_cast<int> (elementPosition);
        auto secondElement = firstElement + 1;
        if (secondElement >= scanPathSize)
            secondElement -= scanPathSize;

        auto firstCoeficient = 1.f - (elementPosition - static_cast<float>(firstElement));
        auto secondCoificient = 1.f - firstCoeficient;

        auto sample = firstCoeficient * wavetables[currentWavetable][firstElement] + secondCoificient * wavetables[currentWavetable][secondElement];

        if (wavefade < 1.f)
        {
            auto sampleNext = firstCoeficient * wavetables[nextWavetable][firstElement] + secondCoificient * wavetables[nextWavetable][secondElement];

            sample = wavefade * sampleNext + (1.f - wavefade) * sample;
            wavefade += wavefadeRate;
            if (wavefade > 1.f)
                currentWavetable = nextWavetable;
        }

        sample = dcBlocker.processSample (sample);
        
        frame[i] += sample * level;

        elementPosition += elementsPerSample;
        if (elementPosition >= static_cast<float>(scanPathSize))
            elementPosition -= static_cast<float>(scanPathSize);
    }
}

void WavetableVoice::setExpectedBlockSize (int blockSize)
{

}

bool WavetableVoice::canPlaySound (SynthesiserSound* sound)
{
    return dynamic_cast<WavetableSound*> (sound) != nullptr;
}

void WavetableVoice::startNote (int midiNoteNumber, float velocity, SynthesiserSound*, int /*currentPitchWheelPosition*/)
{
    level = velocity;
    setFrequency (MidiMessage::getMidiNoteInHertz (midiNoteNumber));
    envelope.noteOn();
}

void WavetableVoice::stopNote (float /*velocity*/, bool allowTailOff)
{
    if (allowTailOff)
    {
        envelope.noteOff();
    }
    else
    {
        // we're being told to stop playing immediately, so reset everything..
        envelope.reset();
        clearCurrentNote();
    }
}

void WavetableVoice::setCurrentPlaybackSampleRate (double newRate)
{
    SynthesiserVoice::setCurrentPlaybackSampleRate (newRate);
    envelope.setSampleRate (newRate);
}

void WavetableVoice::renderNextBlock (AudioBuffer<float>& outputBuffer, int startSample, int numSamples)
{
    if (isVoiceActive() && outputBuffer.getNumChannels() > 0)
    {
        envelopeParams.attack   = attack.load();
        envelopeParams.decay    = decay.load();
        envelopeParams.sustain  = sustain.load();
        envelopeParams.release  = release.load();
        envelope.setParameters (envelopeParams);

        nextWavetable = wavetableIndex.load();
        if (currentWavetable != nextWavetable)
            wavefade = 0.f; //begin crossfade to nextWavetable

        //getNextFrame (outputBuffer.getWritePointer (0, startSample), numSamples);
        //dcBlocker.processFrame (outputBuffer.getWritePointer(0, startSample), numSamples);
        //for (auto i = outputBuffer.getNumChannels(); --i >= 1;)
        //    outputBuffer.copyFrom(i, 0, outputBuffer.getReadPointer(0, startSample), numSamples);
        //envelope.applyEnvelopeToBuffer (outputBuffer, startSample, numSamples);
        //if (!envelope.isActive())
        //    clearCurrentNote();

        while (--numSamples >= 0)
        {
            auto amplitude = envelope.getNextSample();
            auto currentSample = getNextSample();
            for (auto i = outputBuffer.getNumChannels(); --i >= 0;)
                outputBuffer.addSample (i, startSample, currentSample * amplitude);
        
            ++startSample;
        
            if (!envelope.isActive())
                clearCurrentNote();
        }
    }
}

void WavetableVoice::setAttack (double newAttack)
{
    attack = newAttack;

}

void WavetableVoice::setDecay (double newDecay)
{
    decay = newDecay;
}

void WavetableVoice::setSustain (double newSustain)
{
    sustain = newSustain;
}

void WavetableVoice::setRelease (double newRelease)
{
    release = newRelease;
}

GrapheneSynthesiser::GrapheneSynthesiser()
{
    scanPathIndices = GraphenePath::getPath(GraphenePath::PathType::LineDiag1);

    synth.setCurrentPlaybackSampleRate (48000); //set to something sensible to begin

    // Add some voices to our synth, to play the sounds..
    for (auto i = 0; i < polyphony; ++i)
        synth.addVoice (new WavetableVoice (wavetables, wavetableIndex, scanPathIndices.size()));   // These voices will play our custom sine-wave sounds..

    synth.addSound (new WavetableSound());

    scanPathIndices = GraphenePath::getPath (GraphenePath::PathType::Hex5);

    Timer::callAfterDelay (1000, [this]() 
    {
        auto particleCount = grapheneSimulation.getParticleCount();
        if (! particleCount > 0)
        {
            DBG ("Problem Connecting to Simulation");
            return;
        }
        grapheneSimulation.getPositionsCopy (initialPositions);
        calculateCentroid();
        calculateInitialScanPathDistances();

        for (auto& wt : wavetables)
            wt.resize (scanPathIndices.size(), 0.f);
        
        wavetablesReady.store (true);

        wavetableForPlot.resize (scanPathIndices.size(), 0.f);
        grapheneSimulation.addListener (this);

        initialiseFilters();
    });

    simulationParamRanges.push_back (NormalisableRange<float>(0, 10000));
    simulationParamRanges.push_back (NormalisableRange<float>(0.01, 1));
    simulationParamRanges.push_back (NormalisableRange<float>(0.01, 1.5));

    startTimer (33);
}

GrapheneSynthesiser::~GrapheneSynthesiser()
{
    stopTimer();
    grapheneSimulation.removeListener (this);
}

void GrapheneSynthesiser::getPositionsCopy (std::vector<float>& containerForPositionsCopy)
{
    grapheneSimulation.getPositionsCopy (containerForPositionsCopy);
}

void GrapheneSynthesiser::getWavetableCopy (std::vector<float>& containerForWavetableCopy)
{
    std::lock_guard<std::mutex> lg (wavetableForPlotLock);
    containerForWavetableCopy = wavetableForPlot;
}

const std::array<float, 3>& GrapheneSynthesiser::getCentroid() const
{
    return centroid;
}

void GrapheneSynthesiser::prepareToPlay (double newSampleRate, int maximumExpectedSamplesPerBlock)
{
    sampleRate = newSampleRate;
    synth.setCurrentPlaybackSampleRate (sampleRate);
    for (int i = 0; i < synth.getNumVoices(); i++)
    {
        auto voice = dynamic_cast<WavetableVoice*> (synth.getVoice (i));
        if (voice != nullptr)
            voice->setExpectedBlockSize (maximumExpectedSamplesPerBlock);
    }
}

void GrapheneSynthesiser::releaseResources()
{

}

void GrapheneSynthesiser::processBlock (AudioBuffer<float>& buffer, MidiBuffer& midiMessages)
{
    //silence before the wavetable is loaded
    if (! wavetablesReady)
    {
        for (auto i = 0; i < buffer.getNumChannels(); ++i)
            buffer.clear (i, 0, buffer.getNumSamples());
        return;
    }

    synth.renderNextBlock (buffer, midiMessages, 0, buffer.getNumSamples());

    //unpack the midi to send pluck events
    MidiBuffer::Iterator midiIterator (midiMessages);
    MidiMessage midiMessage;
    int midiMessagePosition;
    bool noteReady = midiIterator.getNextEvent (midiMessage, midiMessagePosition);

    for (int sampleCounter = 0; sampleCounter < buffer.getNumSamples(); sampleCounter++)
    {
        if (noteReady && sampleCounter == midiMessagePosition)
        {
            if (midiMessage.isNoteOn())
                grapheneSimulation.pluck (midiMessage.getFloatVelocity() * 2000.f, (int) (midiMessage.getFloatVelocity() * 500.f));
            noteReady = midiIterator.getNextEvent (midiMessage, midiMessagePosition);
        }
    }
}

void GrapheneSynthesiser::setAttack (double newAttack)
{
    for (int i = 0; i < synth.getNumVoices(); i++)
    {
        auto voice = dynamic_cast<WavetableVoice*> (synth.getVoice(i));
        voice->setAttack (newAttack);
    }
}

void GrapheneSynthesiser::setDecay (double newDecay)
{
    for (int i = 0; i < synth.getNumVoices(); i++)
    {
        if (auto voice = dynamic_cast<WavetableVoice*> (synth.getVoice (i)))
            voice->setDecay (newDecay);
    }
}

void GrapheneSynthesiser::setSustain (double newSustain)
{
    for (int i = 0; i < synth.getNumVoices(); i++)
    {
        if (auto voice = dynamic_cast<WavetableVoice*> (synth.getVoice (i)))
            voice->setSustain (newSustain);
    }
}

void GrapheneSynthesiser::setRelease (double newRelease)
{
    for (int i = 0; i < synth.getNumVoices(); i++)
    {
        if (auto voice = dynamic_cast<WavetableVoice*> (synth.getVoice(i)))
            voice->setRelease (newRelease);
    }
}


void GrapheneSynthesiser::setFilterCoefficient(float newCoefficient)
{
    //for (auto filter : filters)
    //{
    //    filter.init(newCoefficient);
    //}
    for (int i = 0; i < filters.size(); i++)
    {
        filters[i].init(newCoefficient);
    }
}

void GrapheneSynthesiser::audioProcessorParameterChanged(AudioProcessor* processor, int parameterIndex, float newValue)
{
    if (parameterIndex == temperatureID)
    {
        temperature = simulationParamRanges[temperatureID].convertFrom0to1(newValue);
    }
    else if (parameterIndex == frictionID)
    {
        friction = simulationParamRanges[frictionID].convertFrom0to1(newValue);
    }
    else if (parameterIndex == timestepID)
    {
        timestep = simulationParamRanges[timestepID].convertFrom0to1(newValue);
    }
}

void GrapheneSynthesiser::audioProcessorChanged (AudioProcessor* processor)
{
}

void GrapheneSynthesiser::hiResTimerCallback()
{
    grapheneSimulation.setTemperature (temperature.load());
    grapheneSimulation.setFriction (friction.load());
    grapheneSimulation.setTimestep (timestep.load());
}

void GrapheneSynthesiser::calculateCentroid()
{
    for (auto index : scanPathIndices)
    {
        auto offset = index * 3;
        centroid[0] += initialPositions[offset];
        centroid[1] += initialPositions[offset + 1L];
        centroid[2] += initialPositions[offset + 2L];
    }
    centroid[0] /= scanPathIndices.size();
    centroid[1] /= scanPathIndices.size();
    centroid[2] /= scanPathIndices.size();
}

void GrapheneSynthesiser::calculateInitialScanPathDistances()
{
    scanPathInitialCentroidDistances.resize (scanPathIndices.size(), 0.f);
    int counter = 0;

    for (auto index : scanPathIndices)
    {
        auto offset = index * 3;
        auto xdist = initialPositions[offset + 0L] - centroid[0];
        auto ydist = initialPositions[offset + 1L] - centroid[1];
        auto zdist = initialPositions[offset + 2L] - centroid[2];

        scanPathInitialCentroidDistances[counter++] = std::sqrtf(xdist * xdist + ydist * ydist + zdist * zdist);
    }
  
    jassert (counter == scanPathIndices.size());
}

void GrapheneSynthesiser::initialiseFilters()
{
    filters.resize (scanPathIndices.size() * 3);
    auto counter = 0;
    auto filterCounter = 0; 
    for (auto index : scanPathIndices)
    {
        auto offset = index * 3L;

        filters[filterCounter + 0].init (0.05);
        filters[filterCounter + 1].init (0.05);
        filters[filterCounter + 2].init (0.05);

        filters[filterCounter + 0].setId (filterCounter + 0);
        filters[filterCounter + 1].setId (filterCounter + 1);
        filters[filterCounter + 2].setId (filterCounter + 2);

        filters[filterCounter + 0].setTargetValue (initialPositions[offset + 0]);
        filters[filterCounter + 1].setTargetValue (initialPositions[offset + 1]);
        filters[filterCounter + 2].setTargetValue (initialPositions[offset + 2]);

        counter++;
        filterCounter += 3;
    }

    jassert(filters.size() == (scanPathIndices.size()*3));
}

void GrapheneSynthesiser::simulationUpdate (const std::vector<float> newPositions)
{
    //calculate displacement into update wavetable
    int counter = 0;
    const int updateIndex = (wavetableIndex + 1) % NumWavetables;
    if (scanPathMethod == DistanceToCentroid)
    {
        for (auto index : scanPathIndices)
        {
            auto offset = index * 3L;
            auto xdist = newPositions[offset + 0] - centroid[0];
            auto ydist = newPositions[offset + 1] - centroid[1];
            auto zdist = newPositions[offset + 2] - centroid[2];
            auto centroidDistance = std::sqrtf (xdist * xdist + ydist * ydist + zdist * zdist);
            wavetables[updateIndex][counter] = centroidDistance - scanPathInitialCentroidDistances[counter];
            counter++;
        }
    }
    else if (scanPathMethod == FilteredVelocity)
    {
        // comparing initial positions to current positions 
        //for (auto index : scanPathIndices)
        //{
        //    auto offset = index * 3;
        //    auto xdist = newPositions[offset + 0] - initialPositions[offset + 0];
        //    auto ydist = newPositions[offset + 1] - initialPositions[offset + 1];
        //    auto zdist = newPositions[offset + 2] - initialPositions[offset + 2];
        //    auto dist = std::sqrtf (xdist * xdist + ydist * ydist + zdist * zdist);
        //    wavetables[updateIndex][counter] = dist;
        //    counter++;
        //}

        // comparing current position to a filtered, delayed version of those positions
        int filterCounter = 0;
        for (auto index : scanPathIndices)
        {
            auto offset = index * 3;

            // update the filters
            auto Xfiltered = filters[filterCounter + 0].getNextSample(newPositions[offset + 0]);
            auto Yfiltered = filters[filterCounter + 1].getNextSample(newPositions[offset + 1]);
            auto Zfiltered = filters[filterCounter + 2].getNextSample(newPositions[offset + 2]);

            // calculate the distance between the current positions and the output of the filters
            auto xdist = newPositions[offset + 0] - Xfiltered;
            auto ydist = newPositions[offset + 1] - Yfiltered;
            auto zdist = newPositions[offset + 2] - Zfiltered;

            auto dist = std::sqrtf (xdist * xdist + ydist * ydist + zdist * zdist);
            
            //if (counter < 3)
            //{
                //DBG("dist: " + (String)dist);
                //DBG("distance for atom " + (String)index + " is calculated using filters " + (String)filters[filterCounter + 0].getID() + ", " + (String)filters[filterCounter+ 1].getID() + " and " + (String)filters[filterCounter + 2].getID());
            //}

            wavetables[updateIndex][counter] = dist * 0.5;
            counter++;
            filterCounter = filterCounter + 3;
        }
    }

    jassert (counter == scanPathIndices.size());
    {
        std::lock_guard<std::mutex> lg (wavetableForPlotLock);
        wavetableForPlot = wavetables[updateIndex];
    }
    wavetableIndex.store (updateIndex);
}
