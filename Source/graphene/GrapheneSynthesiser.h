/*
 * Copyright (c) 2020 Tom Mitchell and Alex Jones.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include "../../JuceLibraryCode/JuceHeader.h"
#include "GrapheneSimulation.h"
#include <array>
#include "../utility/Mappings.h"

enum WavetableConstants { NumWavetables = 5, OversampleAmount = 8 };

enum ScanPathMethod {DistanceToCentroid, FilteredVelocity};

enum ParmIndices { temperatureID, frictionID, timestepID };

//==============================================================================
/** Our demo synth voice just plays a sine wave.. */
class WavetableVoice : public SynthesiserVoice
{
public:
    WavetableVoice() = delete;
    WavetableVoice (std::array<std::vector<float>, NumWavetables>& w, std::atomic<int>& wi, int sps);
    void setFrequency (float newFrequency);
    float getNextSample();
    void getNextFrame(float* frame, int frameSize);
    void setExpectedBlockSize (int blockSize);

    bool canPlaySound (SynthesiserSound* sound) override;
    void startNote (int midiNoteNumber, float velocity, SynthesiserSound*, int /*currentPitchWheelPosition*/) override;
    void stopNote (float /*velocity*/, bool allowTailOff) override;
    void pitchWheelMoved (int /*newValue*/) override {}
    void controllerMoved (int /*controllerNumber*/, int /*newValue*/) override {}
    void setCurrentPlaybackSampleRate (double newRate) override;
    void renderNextBlock (AudioBuffer<float>& outputBuffer, int startSample, int numSamples) override;

    void setAttack  (double newAttack );
    void setDecay   (double newDecay  );
    void setSustain (double newSustain);
    void setRelease (double newRelease);
    
    using SynthesiserVoice::renderNextBlock;

private:
    double level = 0.0;
    int scanPathSize        { 0 };
    float elementsPerSample { 0.f };
    float elementPosition   { 0.f };

    std::array<std::vector<float>, NumWavetables>& wavetables;
    std::atomic<int>& wavetableIndex;
    int currentWavetable { 0 };
    int nextWavetable { 0 };    //wavetable to fade to next
    float wavefade { 1.1f };
    float wavefadeRate {1.f / 240.f}; //fade time 5 ms at 48 kHz
    ADSR envelope;
    ADSR::Parameters envelopeParams;
    std::atomic<float> attack   {0.1f};
    std::atomic<float> decay    {0.2f};
    std::atomic<float> sustain  {0.9f};
    std::atomic<float> release  {0.5f};

    class DcBlocker
    {
    public:
        DcBlocker() = default;
        float processSample (float x)
        {
            auto y = x - xm1 + 0.995f * ym1;
            xm1 = x;
            ym1 = y;
            return y;
        }
        void processFrame (float* frame, int frameSize)
        {
            for (auto i = 0; i < frameSize; i++)
            {
                auto x = frame[i];
                auto y = x - xm1 + 0.995f * ym1;
                xm1 = x;
                ym1 = y;
                frame[i] = y;
            }
        }
    private:
        float xm1{ 0.f };
        float ym1{ 0.f };
    };
    DcBlocker dcBlocker;
    LagrangeInterpolator lagrangeInterpolator; //for audiosample buffer downsampling
};

class GrapheneSynthesiser : private GrapheneSimulation::Listener, public AudioProcessorListener, public HighResolutionTimer
{
public:
    GrapheneSynthesiser();
    ~GrapheneSynthesiser();

    GrapheneSimulation& getGrapheneSimulation() { return grapheneSimulation; }
    Mappings& getMappings() { return mappings; }

    void getPositionsCopy (std::vector<float>& containerForPositionsCopy);
    void getWavetableCopy (std::vector<float>& containerForWavetableCopy);
    const std::array<float, 3>& getCentroid() const;

    void prepareToPlay (double newSampleRate, int maximumExpectedSamplesPerBlock);
    void releaseResources();
    void processBlock (AudioBuffer<float>& buffer, MidiBuffer& midiMessages);

    void setAttack (double newAttack);
    void setDecay (double newDecay);
    void setSustain (double newSustain);
    void setRelease (double newRelease);

    void setFilterCoefficient(float newCoefficient);

    void audioProcessorParameterChanged(AudioProcessor* processor, int parameterIndex, float newValue) override;
    void audioProcessorChanged(AudioProcessor* processor) override;

    void hiResTimerCallback() override;

    std::vector<NormalisableRange<float>> getSimulationParameterRanges() const { return simulationParamRanges; }
private:
    void calculateCentroid();
    void calculateInitialScanPathDistances();

    void initialiseFilters();

    std::atomic<float> temperature;
    std::atomic<float> friction;
    std::atomic<float> timestep;

    void simulationUpdate (const std::vector<float> newPositions) override;

    //======================================================================
    //const int scanPathMethod = DistanceToCentroid;
    const int scanPathMethod = FilteredVelocity;

    std::vector<NormalisableRange<float>> simulationParamRanges;

    class OnePoleFilter
    {
    public:
        OnePoleFilter() = default;
        void setId (int id_) { id = id_; }
        int getId() { return id; }
        void setTargetValue (float newValue)
        {
            previousValue = newValue;
        }
        void init (float coeff)
        {
            a = coeff;
            b = 1.0f - coeff;
        }
        float getNextSample (float input) 
        { 
            return previousValue = (a * input) + (b * previousValue);
        }
    private:
        float a             { 0.0f };
        float b             { 0.0f };
        float previousValue { 0.0f };
        int id              { 0 };
    };

    std::vector<OnePoleFilter> filters;
    //======================================================================

    GrapheneSimulation grapheneSimulation;
    float sampleRate { 44100.f };
    int polyphony { 16 };

    std::vector<float> initialPositions;
    std::vector<int> scanPathIndices;
    std::vector<float> scanPathInitialCentroidDistances;
    std::array<float, 3> centroid { 0.f, 0.f, 0.f };

    std::array<std::vector<float>, NumWavetables> wavetables;
    std::atomic<int> wavetableIndex { 0 };
    std::atomic<bool> wavetablesReady { false };

    std::mutex wavetableForPlotLock;
    std::vector<float> wavetableForPlot;

    Synthesiser synth;

    Mappings mappings;
};
