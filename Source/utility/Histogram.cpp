/*
 * Copyright (c) 2020 Tom Mitchell and Alex Jones.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "Histogram.h"
#include <algorithm>

Histogram::Histogram()
{
}

Histogram::~Histogram()
{
}

std::vector<float> Histogram::getBuckets (const std::vector<float>& source, int totalBuckets)
{
    auto min = *std::min_element (source.begin(), source.end());
    auto max = *std::max_element (source.begin(), source.end());

    std::vector<float> buckets (totalBuckets, 0);

    auto bucketSize = (max - min) / totalBuckets;

    for (auto& value : source)
    {
        auto bucketIndex = 0;
        if (bucketSize > 0.0)
        {
            bucketIndex = (int)((value - min) / bucketSize);
            if (bucketIndex == totalBuckets)
                bucketIndex--;
        }
        buckets[bucketIndex]++;
    }
    return buckets;
}

