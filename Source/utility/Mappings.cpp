/*
 * Copyright (c) 2020 Tom Mitchell and Alex Jones.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "Mappings.h"

Mappings::Mappings() : mappingsData(mainDocTag)
{
	midiToSimulationHashMap.clear();
	simulationToMidiHashMap.clear();

	headerNames = StringArray(idTag, mappingDirectionTag, simulationParameterTag, midiChannelTag, midiTypeTag, midiNumberTag, centreTag, rangeTag, polarityTag, rampTimeInSecTag);

	XmlElement* headers = new XmlElement(headersTag);
	XmlElement* data = new XmlElement(dataTag);

	for (int i = 0; i < headerNames.size(); i++)
	{
		XmlElement* anElement = new XmlElement(columnTag);
		anElement->setAttribute("name", headerNames[i]);

		if (i == 0 || i > 5)
		{
			anElement->setAttribute("width", 60);
		}
		else
		{
			anElement->setAttribute("width", 95);
		}
		anElement->setAttribute("columnId", i + 1);
		
		headers->addChildElement(anElement);
	}

	mappingsData.addChildElement(headers);
	mappingsData.addChildElement(data);
}

Mappings::~Mappings(){}

void Mappings::createNewMapping(Mapping m)
{
	if (m.direction == simulationToMidiMappingTag)
	{			
		if (!simulationToMidiHashMap.count(m.simulationID))
		{
			simulationToMidiHashMap.insert(std::pair<String, Mapping>(m.simulationID, m));
			addToXML(m.midiData, m.simulationID, m.centre, m.range, m.polarity, m.rampTimeInSec, m.direction);
		}
	}
	else if (m.direction == midiToSimulationMappingTag)
	{
		if (!midiToSimulationHashMap.count(m.midiData))
		{
			midiToSimulationHashMap.insert(std::pair<MidiData, Mapping>(m.midiData, m));
			addToXML(m.midiData, m.simulationID, m.centre, m.range, m.polarity, m.rampTimeInSec, m.direction);
		}
	}
}

void Mappings::modifyMapping(Mapping m)
{
	if (m.direction == midiToSimulationMappingTag)
	{
		std::map<MidiData, Mapping, MidiDataCompare>::iterator it = midiToSimulationHashMap.find(m.midiData);

		if (it != midiToSimulationHashMap.end())
		{
			it->second = m;
		}
	}
	else if (m.direction == simulationToMidiMappingTag)
	{
		std::map<String, Mapping, MidiDataCompare>::iterator it = simulationToMidiHashMap.find(m.simulationID);

		if (it != simulationToMidiHashMap.end())
		{
			it->second = m;
		}
	}
}

void Mappings::setFromXML(XmlElement& source)
{
	mappingsData.getChildByName(dataTag)->deleteAllChildElements();

	const XmlElement* data = source.getChildByName(dataTag);

	int numChildren = data->getNumChildElements();

	for (int i = 0; i < numChildren; i++)
	{
		const auto& child = data->getChildElement(i);

		auto d = child->getStringAttribute(mappingDirectionTag);

		// use that value to determine which set of mappings to modify

		auto channel = child->getIntAttribute(midiChannelTag);
		auto number = child->getIntAttribute(midiNumberTag);
		auto type = child->getIntAttribute(midiTypeTag);

		MidiData md = MidiData(number, type, channel);

		auto id = child->getStringAttribute(simulationParameterTag);
		auto c = child->getDoubleAttribute(centreTag);
		auto r = child->getDoubleAttribute(rangeTag);
		auto p = child->getIntAttribute(polarityTag);
		auto rt = child->getDoubleAttribute(rampTimeInSecTag);

		Mapping m = Mapping(id, md, c, r, p, d, rt);

		createNewMapping(m);
	}
}

void Mappings::addToXML(const MidiData& midiData, String simulationParameter, float centre_, float range_, int pol, float rampTimeInSec, String mappingDirection)
{
	int id = mappingsData.getChildByName(dataTag)->getNumChildElements();

	XmlElement* newMapping = new XmlElement(mappingTag);

	newMapping->setAttribute(idTag, id + 1);
	newMapping->setAttribute(mappingDirectionTag, mappingDirection);
	newMapping->setAttribute(simulationParameterTag, simulationParameter);
	newMapping->setAttribute(midiChannelTag, midiData.getChannel());
	newMapping->setAttribute(midiTypeTag, midiData.getType());
	newMapping->setAttribute(midiNumberTag, midiData.getNumber());

	newMapping->setAttribute(centreTag, centre_);
	newMapping->setAttribute(rangeTag, range_);
	newMapping->setAttribute(polarityTag, pol);

	newMapping->setAttribute(rampTimeInSecTag, rampTimeInSec);

	//DBG("nm: " + newMapping->toString());

	mappingsData.getChildByName(dataTag)->addChildElement(newMapping);
}
;