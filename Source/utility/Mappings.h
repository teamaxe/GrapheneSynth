/*
 * Copyright (c) 2020 Tom Mitchell and Alex Jones.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include "../JuceLibraryCode/JuceHeader.h"
#include "../narupacppclient/NarupaClient.h"
#include <unordered_map>
#include <utility>
#include <functional>

static const std::string energyKinetic = { "energy.kinetic" };
static const std::string energyPotential = { "energy.potential" };
static const std::string bondPairs = { "bond.pairs" };
static const std::string chainCount = { "chain.count" };
static const std::string chainNames = { "chain.names" };
static const std::string particleCount = { "particle.count" };
static const std::string particleElements = { "particle.elements" };
static const std::string particleNames = { "particle.names" };
static const std::string particlePositions = { "particle.positions" };
static const std::string particleResidues = { "particle.residues" };
static const std::string residueChains = { "residue.chains" };
static const std::string residueCount = { "residue.count" };
static const std::string residueIds = { "residue.ids" };
static const std::string systemBoxVectors = { "system.box.vectors" };

static const StringArray simulationParams (energyKinetic, energyPotential, bondPairs, chainCount, chainNames, particleCount, particleElements, particleNames, particlePositions, particleResidues, residueChains, residueCount, residueIds, systemBoxVectors);

enum polarity
{
	regular = 1,
	inverted
};

class MidiData
{
public:
	MidiData() { number = type = channel = 1; }
	MidiData(int num_, int type_, int channel_) : number(num_), type(type_), channel(channel_) {}
	MidiData(const MidiData& md) { number = md.getNumber(); type = md.getType(); channel = md.getChannel(); }
	~MidiData() {}
	int getNumber() const { return number; }
	int getChannel() const { return channel; }
	int getType() const { return type; }
private:
	int number;
	int type;
	int channel;
};

class Mapping
{
public:
	Mapping() {}
	Mapping(String id, MidiData midiData_, float centre_, float range_, int pol, String direction_, float rampTimeInSec_) :
		simulationID(id),
		midiData(midiData_),
		centre(centre_),
		range(range_),
		polarity(pol),
		direction(direction_),
		rampTimeInSec(rampTimeInSec_) {}
	Mapping(const Mapping& m2) { simulationID = m2.simulationID; midiData = m2.midiData; centre = m2.centre; range = m2.range; polarity = m2.polarity; direction = m2.direction; rampTimeInSec = m2.rampTimeInSec; }
	~Mapping() {}
	String simulationID = "";
	MidiData midiData;
	float centre = 0.5;
	float range = 1;
	int polarity = 1;
	float rampTimeInSec = 0.1;
	String direction;
};

struct MidiDataCompare
{
	bool operator() (const MidiData& one, const MidiData& two) const
	{
		if ((one.getChannel() == two.getChannel()) && (one.getNumber() == two.getNumber()) && (one.getType() == two.getType()))
		{
			return true;
		}
		else
		{
			return false;
		}
	}
};

static const String mainDocTag = "mappingsData";
static const String headersTag = "headers";
static const String dataTag = "data";
static const String columnTag = "column";
static const String idTag = "id";
static const String mappingTag = "mapping";
static const String simulationToMidiMappingTag = "simToMidi";
static const String midiToSimulationMappingTag = "midiToSim";
static const String mappingDirectionTag = "mappingDirection";
static const String simulationParameterTag = "simParameter";
static const String midiDataTag = "midiData";
static const String centreTag = "centre"; 
static const String rangeTag = "range"; 
static const String polarityTag = "polarity";
static const String midiChannelTag = "midiChannel";
static const String midiTypeTag = "midiType";
static const String midiNumberTag = "midiNumber";
static const String rampTimeInSecTag = "rampTimeInSecTag";

class Mappings
{
public:
	
	enum midiType
	{
		note = 1,
		cc
	};

	Mappings();
	~Mappings();

	std::map<String, Mapping> const getSimulationtoMidiMappings() { return simulationToMidiHashMap; }
	std::map<MidiData, Mapping, MidiDataCompare> const getMidiToSimulationMappings() { return midiToSimulationHashMap; }
	
	void createNewMapping(Mapping m);
	void modifyMapping(Mapping m);

	void setFromXML(XmlElement& source);

	XmlElement& getXMLDescription() { return mappingsData; }
private:
	std::map<String, Mapping> simulationToMidiHashMap;
	std::map<MidiData, Mapping, MidiDataCompare> midiToSimulationHashMap;

	XmlElement mappingsData;

	void addToXML(const MidiData& midiData, String simulationParameter, float centre_, float range_, int pol, float rampTimeInSec, String mappingDirection);

	StringArray headerNames;
};

