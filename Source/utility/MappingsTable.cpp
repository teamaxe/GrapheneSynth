/*
 * Copyright (c) 2020 Tom Mitchell and Alex Jones.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "MappingsTable.h"

MappingsTable::MappingsTable(Mappings& m) : mappings(m)
{
	loadData(mappings.getXMLDescription());

    if (columnList != nullptr)
    {
        forEachXmlChildElement(*columnList, columnXml)
        {
            table.getHeader().addColumn(columnXml->getStringAttribute("name"),
                columnXml->getIntAttribute("columnId"),
                columnXml->getIntAttribute("width"),
                50,
                400,
                TableHeaderComponent::defaultFlags);
        }
    }

    addAndMakeVisible(table);

    createMappingButton.setButtonText("create new mapping");
    createMappingButton.addListener(this);
    addAndMakeVisible(createMappingButton);

    addAndMakeVisible(mappingSelectionMenu);

    table.setColour(ListBox::outlineColourId, Colours::grey); 
    table.setOutlineThickness(1);
    //table.getHeader().setSortColumnId(1, true);   
    table.setMultipleSelectionEnabled(true);
}

MappingsTable::~MappingsTable()
{
    mappingsData.release();
    columnList.release();
    dataList.release();
}

void MappingsTable::loadData(XmlElement& data)
{
    if (data.getTagName() == mainDocTag)
    {
        mappingsData.reset(&data);
        
        dataList.reset(mappingsData->getChildByName(dataTag));
        columnList.reset(mappingsData->getChildByName(headersTag));
        numRows = dataList->getNumChildElements();             

        //DBG("dl: " + dataList.get()->toString());
        //DBG("cl: " + columnList.get()->toString());
    }
}

void MappingsTable::reloadData(XmlElement& data)
{
    forEachXmlChildElement(*mappingsData->getChildByName(dataTag), dataXml)
    {
        int id = dataXml->getIntAttribute(idTag);
        int numChildren = dataList.get()->getNumChildElements();
        DBG(dataList.get()->toString());
        if (id > dataList.get()->getNumChildElements())
        {
            dataList.get()->addChildElement(dataXml);
        }
    }
    table.repaint();
}

void MappingsTable::buttonClicked(Button* b)
{
    Mapping m = mappingSelectionMenu.getCurrentState();
    mappings.createNewMapping(m);
    update();
}

void MappingsTable::update()
{
    numRows = dataList.get()->getNumChildElements();
    table.updateContent();
    table.repaint();
    table.resized();
}

void MappingsTable::modifyMapping(int rowNumber, int columnNumber)
{
    const auto& columnName = table.getHeader().getColumnName(columnNumber);

    //get the value for the cell in that column at this row number 

    const auto& child = dataList->getChildElement(rowNumber);

    auto d = child->getStringAttribute(mappingDirectionTag);

    // use that value to determine which set of mappings to modify

    auto channel = child->getIntAttribute(midiChannelTag);
    auto number = child->getIntAttribute(midiNumberTag);
    auto type = child->getIntAttribute(midiTypeTag);

    MidiData md = MidiData(number, type, channel);

    auto id = child->getStringAttribute(simulationParameterTag);
    auto c = child->getDoubleAttribute(centreTag);
    auto r = child->getDoubleAttribute(rangeTag);
    auto p = child->getIntAttribute(polarityTag);
    auto rt = child->getDoubleAttribute(rampTimeInSecTag);

    Mapping m = Mapping(id, md, c, r, p, d, rt);

    mappings.modifyMapping(m);
}

void MappingsTable::paint(Graphics&)
{

}

void MappingsTable::resized()
{
    float h = (float)getLocalBounds().getHeight();
    Rectangle<int> r = getLocalBounds();
    mappingSelectionMenu.setBounds(r.removeFromBottom(h * 0.1));
    createMappingButton.setBounds(r.removeFromBottom(h * 0.1));
    table.setBounds(r);
}

int MappingsTable::getNumRows()
{
	return numRows;
}

void MappingsTable::paintRowBackground(Graphics& g, int rowNumber, int width, int height, bool rowIsSelected)
{
    auto alternateColour = getLookAndFeel().findColour(ListBox::backgroundColourId)
        .interpolatedWith(getLookAndFeel().findColour(ListBox::textColourId), 0.03f);
    if (rowIsSelected)
        g.fillAll(Colours::lightblue);
    else if (rowNumber % 2)
        g.fillAll(alternateColour);
}

void MappingsTable::paintCell(Graphics& g, int rowNumber, int columnId, int width, int height, bool rowIsSelected)
{
    g.setColour(rowIsSelected ? Colours::darkblue : getLookAndFeel().findColour(ListBox::textColourId)); 
    g.setFont(font);

    if (auto* rowElement = dataList->getChildElement(rowNumber))
    {
        auto text = rowElement->getStringAttribute(getAttributeNameForColumnId(columnId));
        g.drawText(text, 2, 0, width - 4, height, Justification::centredLeft, true); 
    }

    g.setColour(getLookAndFeel().findColour(ListBox::backgroundColourId));
    g.fillRect(width - 1, 0, 1, height);
}

void MappingsTable::sortOrderChanged(int newSortColumnId, bool isForwards)
{
    if (newSortColumnId != 0)
    {
        MappingsDataSorter sorter(getAttributeNameForColumnId(newSortColumnId), isForwards);
        dataList->sortChildElements(sorter);

        table.updateContent();
    }
}

Component* MappingsTable::refreshComponentForCell(int rowNumber, int columnId, bool isRowSelected, Component* existingComponentToUpdate)
{
    if (columnId > 0)
    {
        auto* textLabel = static_cast<EditableTextCustomComponent*> (existingComponentToUpdate);

        if (textLabel == nullptr)
            textLabel = new EditableTextCustomComponent(*this);

        textLabel->setRowAndColumn(rowNumber, columnId);
        return textLabel;
    }

    jassert(existingComponentToUpdate == nullptr);
    return nullptr;   
}

int MappingsTable::getColumnAutoSizeWidth(int columnId)
{
    if (columnId == 9)
        return 50;

    int widest = 32;

    for (int i = getNumRows(); i >= 0; --i)
    {
        if (auto* rowElement = dataList->getChildElement(i))
        {
            auto text = rowElement->getStringAttribute(getAttributeNameForColumnId(columnId));

            widest = jmax(widest, font.getStringWidth(text));
        }
    }

    return widest + 8;
}

MappingSelectionMenu::MappingSelectionMenu()
{
    //"mappingDirection", "simParameter", "midiChannel", "midiType", "midiNumber", "centre", "range", "polarity"
    for (int i = 0; i < parameterNames.size(); i++)
    {
        ComboBox* aComboBox = new ComboBox(parameterNames[i]);
        parameterBoxes.add(aComboBox);

        Label* aLabel = new Label(parameterNames[i], parameterNames[i]);
        parameterLabels.add(aLabel);
    }

    parameterBoxes[mappingDirection]->addItemList(StringArray(simulationToMidiMappingTag, midiToSimulationMappingTag), 1);
    parameterBoxes[simParameter]->addItemList(simulationParams, 1);
    
    for (int i = 1; i <= 127; i++) { parameterBoxes[midiNumber]->addItem(String(i), i); }
    for (int i = 1; i <= 16; i++) { parameterBoxes[midiChannel]->addItem(String(i), i); }

    parameterBoxes[midiType]->addItemList(StringArray("Note", "CC"), 1);
    parameterBoxes[polarity]->addItemList(StringArray("Regular", "Inverted"), 1);

    float val = 0.1;
    for (int i = 0; i < 10; i++)
    {
        parameterBoxes[range]->addItem((String)val, i+1);
        parameterBoxes[centre]->addItem((String)val, i+1);
        parameterBoxes[rampTimeInSec]->addItem((String)val, i+1);
        val += 0.1f;
    }

    for (int i = 0; i < parameterBoxes.size(); i++)
    {
        addAndMakeVisible(parameterBoxes[i]);
        addAndMakeVisible(parameterLabels[i]);
    }
}

MappingSelectionMenu::~MappingSelectionMenu()
{
}

void MappingSelectionMenu::paint(Graphics& g)
{
}

void MappingSelectionMenu::resized()
{
    Rectangle<int> r = getLocalBounds();

    float w = (float)r.getWidth() / (float)parameterBoxes.size();
    float h = (float)r.getHeight() * 0.5f;

    Rectangle<int> labelArea = r.removeFromTop(h);
    Rectangle<int> boxArea = r;

    for (int i = 0; i < parameterBoxes.size(); i++)
    {
        if (i < parameterBoxes.size() - 1)
        {
            parameterBoxes[i]->setBounds(boxArea.removeFromLeft(w));
            parameterLabels[i]->setBounds(labelArea.removeFromLeft(w));
        }
        else
        {
            parameterBoxes[i]->setBounds(boxArea);
            parameterLabels[i]->setBounds(labelArea);
        }
    }
}

void MappingSelectionMenu::comboBoxChanged(ComboBox* comboBoxThatHasChanged)
{

}

Mapping MappingSelectionMenu::getCurrentState()
{
    MidiData md = MidiData(parameterBoxes[midiNumber]->getText().getIntValue(), parameterBoxes[midiType]->getSelectedId(), parameterBoxes[midiChannel]->getText().getIntValue());
    Mapping state = Mapping(parameterBoxes[simParameter]->getText(), 
                            md, 
                            parameterBoxes[centre]->getText().getDoubleValue(), 
                            parameterBoxes[range]->getText().getDoubleValue(), 
                            parameterBoxes[polarity]->getSelectedId(),
                            parameterBoxes[mappingDirection]->getText(),
                            parameterBoxes[rampTimeInSec]->getText().getDoubleValue());
    return state;
}
