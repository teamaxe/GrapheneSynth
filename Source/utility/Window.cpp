/*
 * Copyright (c) 2020 Tom Mitchell and Alex Jones.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "Window.h"
#include <numeric>

Window::Window(int size)
{
    buffer.resize(bufferSize);
}

Window::~Window()
{

}

float Window::getNormalisedValue(float nextValue)
{
    if (isnan<float>(nextValue) || nextValue == 0)
        return 0.0;

    std::vector<float>::iterator it = buffer.begin() + bufferWritePos;
    buffer.insert(it, nextValue);

    bufferWritePos++;

    auto normalisedValue = 0.0;

    if (bufferWritePos > bufferSize - 1)
    {
        bufferWritePos = 0;
        distribution = histogram.getBuckets(buffer, 20);
        mean = 0.0f;
        standardDeviation = 0.0f;

        auto size = (float)buffer.size();

        if (size > 0)
        {
            mean = std::accumulate(buffer.begin(), buffer.end(), 0.f) / size;

            auto sum = std::accumulate(buffer.begin(), buffer.end(), 0.f);
            mean = sum / size;

            auto sq_sum = std::inner_product(buffer.begin(), buffer.end(), buffer.begin(), 0.f);
            standardDeviation = std::sqrtf(sq_sum / size - mean * mean);
        }

        min = mean - (2.0f * standardDeviation);
        max = mean + (2.0f * standardDeviation);

        go = true;
    }

    if (go)
        normalisedValue = (nextValue - min) / (max - min);

    if (normalisedValue > 1.0)
        normalisedValue = 1.0;
    else if (normalisedValue < 0.0)
        normalisedValue = 0.0;

    return normalisedValue;
}
