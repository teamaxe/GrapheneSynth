/*
 * Copyright (c) 2020 Tom Mitchell and Alex Jones.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once
#include <vector>
#include "Histogram.h" 

#include "../JuceLibraryCode/JuceHeader.h"

class Window
{
public:
    Window (int size);
    ~Window();

    float getNormalisedValue (float nextValue);

private:
    Window() {}
    Histogram histogram;
    std::vector<float> buffer;
    bool go                         = false;
    const int bufferSize            = 50;
    int bufferWritePos              = 0;
    float standardDeviation         = 0.f;
    float mean                      = 0.f;
    float min                       = 0.f;
    float max                       = 0.f;

    std::vector<float> distribution;
};
